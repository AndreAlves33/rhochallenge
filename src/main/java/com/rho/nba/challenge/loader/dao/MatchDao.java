package com.rho.nba.challenge.loader.dao;

import com.rho.nba.challenge.common.interfaces.DaoInterface;
import com.rho.nba.challenge.loader.model.Comment;
import com.rho.nba.challenge.loader.model.Match;
import org.springframework.data.domain.Pageable;

import java.util.List;

public abstract  class MatchDao implements DaoInterface<Match> {
    public abstract void addAll(List<Match> o);
    public abstract List<Match> getAllByDate(Pageable pageable, String date);
    public abstract List<Comment> getAllCommentsOrdered(Long id);
    public abstract Comment addCommentToMatch(Long id, Comment comment);
    public abstract Comment deleteCommentFromMatch(Long id, Integer commentId);
    public abstract Comment updateCommentFromMatch(Long id, Comment comment);
}
