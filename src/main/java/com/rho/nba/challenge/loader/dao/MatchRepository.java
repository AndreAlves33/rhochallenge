package com.rho.nba.challenge.loader.dao;

import com.rho.nba.challenge.loader.model.Match;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


@Repository
public interface MatchRepository extends JpaRepository<Match, Long> {
    List<Match> findByDate(@Param("date") Date date);
}
