package com.rho.nba.challenge.loader.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "comments")
public class Comment implements Comparable<Comment>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "match_id", nullable = false)
    @JsonBackReference
    private Match match;

    @Column(name = "date_time")
    @NotNull
    private ZonedDateTime dateTime;


    @Column(name = "comment")
    @NotNull
    private String comment;

    @Override
    public int compareTo(Comment comment) {
        if (comment == null || comment.getDateTime() == null) {
            return 0;
        }
        return dateTime.compareTo(comment.getDateTime());
    }

    @Override
    public String toString() {
        return "Comment{" +
                "match=" + match +
                ", dateTime=" + dateTime +
                ", comment='" + comment + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment1 = (Comment) o;
        return id == comment1.id &&
                Objects.equals(dateTime, comment1.dateTime) &&
                Objects.equals(comment, comment1.comment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dateTime, comment);
    }
}
