package com.rho.nba.challenge.loader.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "matches")

@NamedQuery(name = "Match.findByDate",
        query = "SELECT m FROM Match m WHERE DATE_TRUNC('day', date_time) = :date"
)
public class Match implements Serializable {

    @Column(name = "match_id")
    @NotNull
    @Id
    private Long match_id;

    @Column(name = "date_time")
    @NotNull
    private ZonedDateTime dateTime;
    @Column(name = "home_team_name")
    @NotNull
    private String homeTeamName;
    @Column(name = "away_team_name")
    @NotNull
    private String awayTeamName;
    @Column(name = "home_team_score")
    @NotNull
    private Integer homeTeamScore;
    @Column(name = "away_team_score")
    @NotNull
    private Integer awayTeamScore;

    @OneToMany(mappedBy = "match", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<Comment> comments;


    @Override
    public String toString() {
        return "Match{" +
                ", dateTime=" + dateTime +
                ", homeTeamName='" + homeTeamName + '\'' +
                ", awayTeamName='" + awayTeamName + '\'' +
                ", homeTeamScore=" + homeTeamScore +
                ", awayTeamScore=" + awayTeamScore +
                ", comments=" + comments +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Match match = (Match) o;
        return Objects.equals(match_id, match.match_id) &&
                Objects.equals(dateTime, match.dateTime) &&
                Objects.equals(homeTeamName, match.homeTeamName) &&
                Objects.equals(awayTeamName, match.awayTeamName) &&
                Objects.equals(homeTeamScore, match.homeTeamScore) &&
                Objects.equals(awayTeamScore, match.awayTeamScore);
    }

    @Override
    public int hashCode() {
        return Objects.hash(match_id, dateTime, homeTeamName, awayTeamName, homeTeamScore, awayTeamScore);
    }
}
