package com.rho.nba.challenge.loader.dao;

import com.rho.nba.challenge.common.exceptions.BadRequestException;
import com.rho.nba.challenge.common.exceptions.ResourceNotFoundException;
import com.rho.nba.challenge.common.utils.Util;
import com.rho.nba.challenge.loader.model.Comment;
import com.rho.nba.challenge.loader.model.Match;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.*;

@Service
public class MatchDaoImp extends MatchDao {

    private static final Logger log = LoggerFactory.getLogger(MatchDaoImp.class);

    @Autowired
    private MatchRepository matchRepository;

    @Override
    public Page<Match> getAll(Pageable pageable) {
        return matchRepository.findAll(pageable);
    }

    @Override
    public List<Match> getAllByDate(Pageable pageable, String date) {
        try {
            return matchRepository.findByDate(Util.paseDate(date));
        } catch (ParseException e) {
            log.error("Unable to parse date",e);
        }
        throw new BadRequestException("Invalid date!");
    }

    @Override
    public List<Comment> getAllCommentsOrdered(Long id) {
        Match existingMatch = checkIfIdIsPresentandReturnAuthor(id);
        List<Comment> comments =  existingMatch.getComments();
        Collections.reverse(comments);
        return comments;
    }

    @Override
    public Match add(Match match) {
        return matchRepository.save(match);
    }

    @Override
    public Match update(Match match, long id) {
        Match existentMatch = checkIfIdIsPresentandReturnAuthor(id);
        existentMatch.setDateTime(match.getDateTime());
        existentMatch.setAwayTeamScore(match.getAwayTeamScore());
        existentMatch.setAwayTeamName(match.getAwayTeamName());
        existentMatch.setHomeTeamName(match.getHomeTeamName());
        existentMatch.setHomeTeamScore(match.getHomeTeamScore());
        existentMatch.setComments(match.getComments());
        return matchRepository.save(existentMatch);
    }

    @Override
    public Match getById(long id) {
        return checkIfIdIsPresentandReturnAuthor(id);
    }

    @Override
    public Match deleteById(long id) {
        Match existingMatch = checkIfIdIsPresentandReturnAuthor(id);
        matchRepository.delete(existingMatch);
        return existingMatch;
    }

    @Override
    public void addAll(List<Match> matches) {
        matchRepository.saveAll(matches);
    }

    @Override
    public Comment addCommentToMatch(Long id, Comment comment) {
        Match existingMatch = checkIfIdIsPresentandReturnAuthor(id);
        comment.setMatch(existingMatch);
        List<Comment> comments = existingMatch.getComments();
        if (comments == null) {
            comments = new ArrayList<>();
        }
        comments.add(comment);
        matchRepository.save(existingMatch);
        return comment;
    }

    @Override
    public Comment deleteCommentFromMatch(Long id, Integer commentId) {
        Match existingMatch = checkIfIdIsPresentandReturnAuthor(id);
        List<Comment> comments = existingMatch.getComments();
        Comment comment = null;
        if (comments != null && !comments.isEmpty()) {
            Iterator<Comment> commentIterator = comments.iterator();
            while (commentIterator.hasNext()) {
                comment = commentIterator.next();
                if (comment.getId() == commentId) {
                    commentIterator.remove();
                }
            }
        }
        if (comment == null) {
            throw new ResourceNotFoundException(" Comment id = " + commentId + " not found");
        }
        matchRepository.save(existingMatch);
        return comment;
    }

    @Override
    public Comment updateCommentFromMatch(Long id, Comment newComment) {
        Match existingMatch = checkIfIdIsPresentandReturnAuthor(id);
        List<Comment> comments = existingMatch.getComments();
        Comment comment = null;
        if (comments != null && !comments.isEmpty()) {
            Iterator<Comment> commentIterator = comments.iterator();
            while (commentIterator.hasNext()) {
                comment = commentIterator.next();
                if (comment.getId() == newComment.getId()) {
                    comment.setComment(newComment.getComment());
                    comment.setDateTime(newComment.getDateTime());
                }
            }
        }
        if (comment == null) {
            throw new ResourceNotFoundException(" Comment id = " + newComment.getId() + " not found");
        }
        matchRepository.save(existingMatch);
        return comment;
    }

    private Match checkIfIdIsPresentandReturnAuthor(long id) {
        if (!matchRepository.findById(id).isPresent())
            throw new ResourceNotFoundException(" Match id = " + id + " not found");
        else
            return matchRepository.findById(id).get();
    }
}
