package com.rho.nba.challenge;

import com.rho.nba.challenge.reader.ApiLookupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

@SpringBootApplication
@EnableAsync
public class NBAApplication implements CommandLineRunner{

    private static final Logger log = LoggerFactory.getLogger(NBAApplication.class);

    @Autowired
    private ApiLookupService apiLookupService;

    /**
     * Starts spring boot application
     * @param args
     */
    public static void main( String[] args ) {
        log.info("Starting NBA RHO Api");
        SpringApplication.run( NBAApplication.class, args );
    }

    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor()
    {
        return new MethodValidationPostProcessor();
    }


    @Override
    public void run(String...args) {
        if(args.length > 0 && args[0].equals("fillDB")){
            apiLookupService.startProcessing();
        }
    }
}
