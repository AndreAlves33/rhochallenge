package com.rho.nba.challenge.reader.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class MatchStats {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("date")
    private String date;
    @JsonProperty("home_team")
    private Team homeTeam;
    @JsonProperty("home_team_score")
    private Integer homeTeamScore;
    @JsonProperty("period")
    private Integer period;
    @JsonProperty("postseason")
    private boolean postseason;
    @JsonProperty("season")
    private Integer season;
    @JsonProperty("status")
    private String status;
    @JsonProperty("time")
    private String time;
    @JsonProperty("visitor_team")
    private Team visitorTeam;
    @JsonProperty("visitor_team_score")
    private Integer visitorTeamScore;

    @Override
    public String toString() {
        return "Match{" +
                "id=" + id +
                ", date='" + date + '\'' +
                ", homeTeamScore=" + homeTeamScore +
                ", period=" + period +
                ", postseason=" + postseason +
                ", season=" + season +
                ", status='" + status + '\'' +
                ", time='" + time + '\'' +
                ", visitorTeamScore=" + visitorTeamScore +
                '}';
    }
}
