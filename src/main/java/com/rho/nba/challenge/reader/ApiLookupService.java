package com.rho.nba.challenge.reader;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rho.nba.challenge.reader.model.MatchStats;
import com.rho.nba.challenge.reader.model.PaginationPage;
import com.rho.nba.challenge.common.httpclient.HttpClient;
import com.rho.nba.challenge.loader.dao.MatchDao;
import com.rho.nba.challenge.loader.model.Match;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ApiLookupService {

    private final static Logger log = LoggerFactory.getLogger(ApiLookupService.class);

    private final static String BASE_URL = "https://free-nba.p.rapidapi.com";
    private final static String GAMES_URL = "/games";

    private final static String HEADER_NAME_API_KEY = "x-rapidapi-key";
    private final static String HEADER_VALUE_API_KEY = "5f0549b1b2msh985ffa24efbbba1p16e0eajsnf55a84ec4e22";

    private final static String HEADER_NAME_API_HOST = "x-rapidapi-host";
    private final static String HEADER_VALUE_API_HOST  = "free-nba.p.rapidapi.com";

    private final static String HEADER_NAME_API_PROJECT_NAME = "rapidapiprojectname";
    private final static String HEADER_VALUE_API_PROJECT_NAME = "default-application_4076238";

    private final static Integer RESULTS_PER_PAGE = 100;

    private final HttpClient httpClient;

    private final MatchDao matchDao;

    public ApiLookupService(MatchDao matchDao){
        this.httpClient = startHttpClient();
        this.matchDao = matchDao;
    }

    /**
     * Reads nba free api - wais 1000 ms to avoid too many requests error
     * @param page
     * @param perPage
     * @return
     */
    private PaginationPage<MatchStats> readStatsPage(Integer page, Integer perPage){
        PaginationPage<MatchStats> matches = null;
        try {
            matches = new ObjectMapper().readValue(httpClient.get(GAMES_URL+getPageQueryParam(page,perPage)), new TypeReference<PaginationPage<MatchStats>>() {});
            //To avoid too many requests error
            Thread.sleep(1000);
        } catch (IOException | InterruptedException e) {
            log.error("Error getting page from API",e);
        }
        return matches;
    }

    private String getPageQueryParam(Integer page, Integer perPage) {
        StringBuilder url = new StringBuilder("?");
        url.append("page=");
        url.append(page);
        url.append("$per_page=");
        url.append(perPage);
        return url.toString();
    }

    private HttpClient startHttpClient() {
        HttpClient httpClient = new HttpClient(BASE_URL);
        httpClient.addHeader(HEADER_NAME_API_KEY,HEADER_VALUE_API_KEY);
        httpClient.addHeader(HEADER_NAME_API_HOST,HEADER_VALUE_API_HOST);
        httpClient.addHeader(HEADER_NAME_API_PROJECT_NAME,HEADER_VALUE_API_PROJECT_NAME);
        return httpClient;
    }

    @Async
    public void startProcessing() {
        log.info("STARTING READING API");
        PaginationPage<MatchStats> page = null;
        while (true){
            page = readStatsPage(page == null ? 1 : page.getMeta().getNextPage(),RESULTS_PER_PAGE);
            List<Match> matches = new ArrayList<>();
            page.getData().forEach(match -> matches.add(transformApiMatch(match)));
            matchDao.addAll(matches);
            if(page.getMeta().getTotalPages().equals(page.getMeta().getCurrentPage())){
                break;
            }
        }
    }

    private static Match transformApiMatch(MatchStats matchStats){
        Match match = new Match();
        match.setMatch_id(matchStats.getId());
        match.setHomeTeamName(matchStats.getHomeTeam().getName());
        match.setHomeTeamScore(matchStats.getHomeTeamScore());
        match.setAwayTeamName(matchStats.getVisitorTeam().getName());
        match.setAwayTeamScore(matchStats.getVisitorTeamScore());
        match.setDateTime(ZonedDateTime.parse(matchStats.getDate()));
        return match;
    }
}
