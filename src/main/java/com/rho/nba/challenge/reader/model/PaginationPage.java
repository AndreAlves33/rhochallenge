package com.rho.nba.challenge.reader.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaginationPage<T> {
    private List<T> data;
    private Meta meta;
}
