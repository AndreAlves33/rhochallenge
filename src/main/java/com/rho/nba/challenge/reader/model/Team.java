package com.rho.nba.challenge.reader.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Team {
    @JsonProperty("id")
    private Long id;
    @JsonProperty("abbreviation")
    private String abbreviation;
    @JsonProperty("city")
    private String city;
    @JsonProperty("conference")
    private String conference;
    @JsonProperty("division")
    private String division;
    @JsonProperty("full_name")
    private String fullName;
    @JsonProperty("name")
    private String name;
}
