package com.rho.nba.challenge.service;

import com.rho.nba.challenge.common.exceptions.BadRequestException;
import com.rho.nba.challenge.common.model.ResponseWrapper;
import com.rho.nba.challenge.loader.dao.MatchDao;
import com.rho.nba.challenge.loader.model.Comment;
import com.rho.nba.challenge.loader.model.Match;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@Validated
@RestController
@RequestMapping("/api/v1/matches")
public class MatchesApi {

    private static final Logger log = LoggerFactory.getLogger(MatchesApi.class);

    @Autowired
    private MatchDao matchDao;

    /**
     * Gets all matches with pagination
     * @param pageable
     * @return
     */
    @GetMapping(value = "/all")
    public ResponseWrapper<Page<Match>> getAllMatches(Pageable pageable) {
        log.info("Get all matches - start");
        try {
            return new ResponseWrapper<>(matchDao.getAll(pageable), HttpStatus.OK);
        } finally {
            log.info("Get all matches - finish");
        }
    }

    /**
     * Gets all matches for a given date
     * @param pageable
     * @param date format yyyy-MM-dd
     * @return
     */
    @GetMapping()
    public ResponseWrapper<List<Match>> getAllByDate(Pageable pageable, @RequestParam String date) {
        log.info("Get all matches for a date - start");
        try {
            if(date == null || date.isEmpty()){
                throw new BadRequestException("Date can not be null!");
            }
            return new ResponseWrapper<>(matchDao.getAllByDate(pageable, date), HttpStatus.OK);
        } finally {
            log.info("Get all matches for a date - finish");
        }
    }

    /**
     * Gets a match with a specific id
     * @param id
     * @return
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<Match> getMatchById(@PathVariable(value = "id") String id) {
        log.info("Get match by id id={} - start", id);
        try {
            return new ResponseWrapper<>(matchDao.getById(Long.parseLong(id)), HttpStatus.OK);
        } finally {
            log.info("Get match by id id - finish");
        }
    }

    /**
     * Gets comments for a match
     * @param id
     * @return
     */
    @GetMapping(value = "/{id}/comments")
    public ResponseEntity<List<Comment>> getMatchComments(@PathVariable(value = "id") String id) {
        log.info("Get comments for match by id id={} - start", id);
        try {
            return new ResponseWrapper<>(matchDao.getAllCommentsOrdered(Long.parseLong(id)), HttpStatus.OK);
        } finally {
            log.info("Get comments for match by id - finish");
        }
    }

    /**
     * Adds a comments to a match
     * @param id
     * @param comment
     * @return
     */
    @PostMapping(value = "/{id}/comments")
    public ResponseEntity<Comment> addMatchComment(@PathVariable(value = "id") String id, @Valid @RequestBody Comment comment) {
        log.info("Add comment to match with id id={} - start", id);
        try {
            return new ResponseWrapper<>(matchDao.addCommentToMatch(Long.parseLong(id), comment), HttpStatus.OK);
        } finally {
            log.info("Add comment to match with id - finish");
        }
    }

    /**
     * Deletes a comment from a match
     * @param id
     * @param commentId
     * @return
     */
    @DeleteMapping(value = "/{id}/comments/{commentId}")
    public ResponseEntity<Comment> deleteMatchComments(@PathVariable(value = "id") String id, @PathVariable(value = "commentId") String commentId) {
        log.info("Delete comment from match with id id={} - start", id);
        try {
            return new ResponseWrapper<>(matchDao.deleteCommentFromMatch(Long.parseLong(id), Integer.parseInt(commentId)), HttpStatus.OK);
        } finally {
            log.info("Delete comment from match with id - finish");
        }
    }

    /**
     * Updates a comment for a given match
     * @param id
     * @param comment
     * @return
     */
    @PatchMapping(value = "/{id}/comments/{commentId}")
    public ResponseEntity<Comment> updateMatchComments(@PathVariable(value = "id") String id, @Valid @RequestBody Comment comment) {
        log.info("Update comment from match with id id={} - start", id);
        try {
            return new ResponseWrapper<>(matchDao.updateCommentFromMatch(Long.parseLong(id), comment), HttpStatus.OK);
        } finally {
            log.info("Update comment from match with id - finish");
        }
    }

}
