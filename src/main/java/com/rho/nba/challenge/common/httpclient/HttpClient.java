package com.rho.nba.challenge.common.httpclient;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class HttpClient {

    private final String server;
    private final RestTemplate rest;
    private final HttpHeaders headers;
    private HttpStatus status;

    public HttpClient(String baseUrl){
        this.rest = new RestTemplate();
        this.headers = new HttpHeaders();
        this.server = baseUrl;
    }

    public String get(String uri) {
        HttpEntity<String> requestEntity = new HttpEntity<>("", headers);
        ResponseEntity<String> responseEntity = rest.exchange(server + uri, HttpMethod.GET, requestEntity, String.class);
        this.setStatus(responseEntity.getStatusCode());
        return responseEntity.getBody();
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public void addHeader(String name, String value){
        this.headers.add(name,value);
    }
}
