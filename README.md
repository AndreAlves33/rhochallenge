# RhoChallenge
Free nba-api data transformer and parser

## Usage

### Start postgresSQL docker
```
cd RhoChallenge/docker
docker-compose up -d 
```
#### Build and run spring boot application
```
cd RhoChallenge/docker
docker-compose up -d 
./gradlew clean build
./gradlew bootRun -Pargs=fillDB
```
